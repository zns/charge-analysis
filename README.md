## Analysis of Charge

`hetfeaturecalcium.gp` created using: Search http://www.ncbi.nlm.nih.gov/protein/ with "(HET[Feature key]) AND (Calcium OR CA)". Then Send to: -> File -> GenPept(full)


`hetfeature.gp` created using Search http://www.ncbi.nlm.nih.gov/protein/ with "HET[Feature key] " then Send to: -> File -> GenPept(full)


`sequences.gp` created using Search `"het"[Feature key] OR "Het"[Feature key] OR "HET"[Feature key] OR Ca2[Title] OR Calcium[Title] OR ca2[Title] OR calcium[Title] ` then Send to: -> File -> GenPept(full

```bash
python run.py -> generates results.json
python analyze.py -> generates data.json, urls.txt, highlycharged.json
# python filter.py -> DEPRECATED dedupes the pdbs, generates resultsFiltered.json, data.json, urls.txt, highlycharged.json (used instead of analyze.py)
python3 analyzePy3.py -> generates resultsPDBs.json, getting all PDBs information for stuff in results.json (do once)
```
# Papers to check out

Stability, homodimerization, and calcium-binding properties of a single, variant beta gamma-crystallin domain of the protein absent in melanoma 1 (AIM1)
Calcium binding peptides from alpha-lactalbumin: Implications for protein folding and stability 
Calcium-binding crystallins from Yersinia pestis - Characterization of two single beta gamma-crystallin domains of a putative exported protein 
Calcium-Induced Folding of a Beta Roll Motif Requires C-Terminal Entropic Stabilization
DEPLETION OF CELLULAR CALCIUM ACCELERATES PROTEIN-DEGRADATION IN THE ENDOPLASMIC-RETICULUM

# Important papers

search TOPIC: (protein fold* calcium stabil*) in web of science

sort by relevance

stopped on 91


--

-0.363

1wza

[](http://www.sciencedirect.com/science/article/pii/S0141022913001907)

Half-life in Fig.5 show that half-life increases with removing charge. wildtype < N->D

---

-0.1666

1e5n / 1clx

[Calcium Protects a Mesophilic Xylanase from Proteinase Inactivation and Thermal Unfolding](http://www.jbc.org/content/272/28/17523.long)

> These data indicate that the binding of calcium to XYLA causes a gross stabilization of the protein which results in a decrease in the thermal inactivation of the enzyme. Similarly, replacing the three amino acids that bind to the metal ion with alanine stabilizes the three-dimensional structure of the xylanase against thermal unfolding in the absence of calcium

!!!! THIS ONE HAS CHARGE DESTABILIZATION??

---

-0.36

[5hdb, Metal ion binding properties and conformational states of calcium- and integrin-binding protein](http://pubs.acs.org/doi/full/10.1021/bi035432b)

>  Our results indicate that in the absence of any bound metal ions, apo-CIB adopts a folded yet highly flexible molten globule-like structure. Both calcium and magnesium binding induce conformational changes which stabilize both the secondary and tertiary structure of CIB, resulting in considerable increases in the thermal stability of the proteins

[Domain stability and metal-induced folding of calcium- and integrin-binding protein 1](http://pubs.acs.org/doi/abs/10.1021/bi700200z)

> metal-induced conformational changes in CIB1

---

-0.18

[3pow, Calreticulin Is a Thermostable Protein with Distinct Structural Responses to Different Divalent Cation Environments](http://www.jbc.org/content/286/11/8771.long)

> these studies indicate interactions between the globular and acidic domains of calreticulin that are impacted by divalent cations. These interactions influence the structure and stability of calreticulin, and are likely to determine the multiple functional activities of calreticulin in different subcellular environments

---

-0.17

[3zqk,Calcium stabilizes the von Willebrand factor A2 domain by promoting refolding](http://www.pnas.org/content/109/10/3742.long)

> we found that calcium did not affect VWF A2 unfolding, but rather enhanced refolding kinetics fivefold, resulting in a 0.9 kcal/mol stabilization in the folding activation energy in the presence of calcium

---

NO PDB.

Calcium-induced Folding of Intrinsically Disordered Repeat-in-Toxin (RTX) Motifs via Changes of Protein Charges and Oligomerization States [paper](http://www.jbc.org/content/286/19/16997.long)

> RTX proteins are natively disordered in the absence of calcium but fold upon calcium binding

---

-.66

Human Hsp70 molecular chaperone binds two calcium ions within the ATPase domain [paper](http://www.ncbi.nlm.nih.gov/pubmed?Db=pubmed&Cmd=Retrieve&list_uids=9083109&dopt=abstractplus)

> calcium-binding motif that can play a role in the stabilization of protein structure

```
>>> a['3iuc']  # HUMAN HSP70
{u'chargedensity': [-0.3333333333333333, -0.6666666666666666], u'ca_region': [[249, 254], [312]], u'title': u"Crystal structures of the ATPase domains of four human Hsp70 isoforms: HSPA1L/Hsp70-hom, HSPA2/Hsp70-2, HSPA6/Hsp70B', and HSPA5/BiP/GRP78", u'authors': u'Wisniewska,M., Karlberg,T., Lehtio,L., Johansson,I., Kotenyova,T., Moche,M. and Schuler,H.', u'decription': u'Chain C, Crystal Structure Of The Human 70kda Heat Shock Protein 5 (BipGRP78) ATPASE DOMAIN IN COMPLEX WITH ADP.', u'source': u'Homo sapiens (human)', u'db_source': u'pdb: molecule 3IUC, chain 67, release Feb 3, 2010; deposition: Aug 31, 2009; class: Chaperone; source: Mmdb_id: 76914, Pdb_id 1: 3IUC; Exp. method: X-Ray Diffraction.', u'gi': u'259090350'}
```

---

-0.666

[2FWs_A](http://www.ncbi.nlm.nih.gov/pubmed/16600866)
> Strikingly, in the absence of Ca2+, the upper half of CBD1 unfolds while CBD2 maintains its structural integrity

---

-0.27
[1O8J_A](http://www.ncbi.nlm.nih.gov/pubmed/?term=1O8J)
> Given the pH dependence of Ca(2+) affinity, PelC activity at pH 4.5 has been reexamined. At saturating Ca(2+) concentrations, PelC activity increases 10-fold at pH 4.5 but is less than 1% of maximal activity at pH 9.5. Taken together, the studies suggest that the primary Ca(2+) ion in PelC has multiple functions


---

-0.5

[2clt](http://onlinelibrary.wiley.com.proxy.lib.duke.edu/doi/10.1002/prot.340120106/pdf), references from [original paper](http://www.jbc.org/content/271/14/8015.full#ref-30)
> Calcium provides stabilization for MMP

---

-0.44

[2ZWO](http://www.ncbi.nlm.nih.gov/pubmed/19813760)
> It requires Ca2+ for folding and assumes a molten globule-like structure in the absence of Ca2+ even in the presence of Tk-propeptide
> This loop is probably unfolded in the absence of Ca2+, due to extensive negative charge repulsions among aspartic acid residues. However, in the presence of Ca2+, these aspartic acid residues come close with one another to form Ca2+-binding sites and directly coordinate with the Ca2+ ions at these sites

---

-0.416

[4l41](http://www.ncbi.nlm.nih.gov/protein/4L41_A), or [α-lactalbumin](http://www.sciencedirect.com/science/article/pii/S0014579300015465)
> The binding of Ca2+ to α-LA causes pronounced changes in structure and function, mostly in tertiary, but not secondary, structure

see also "Calcium binding peptides from alpha-lactalbumin: Implications for protein folding and stability "

---

-0.666

[4lty, Characterization of Two Human Skeletal Calsequestrin Mutants Implicated in Malignant Hyperthermia and Vacuolar Aggregate Myopathy.](http://www.jbc.org.proxy.lib.duke.edu/content/290/48/28665.long)
> The D244G mutation loses Ca2+, resulting in structural instability. M87T inhibits polymerization of calsequestrin by altering the Casq1 dimer interface.

---


## NOTES

YOu can increase stability by mutating to negatiely charged residue, if you have calcium. Otherwise, if you don't have calcium, youre better off mutating all the residues to non-negative so that they don't repulse and it gives you stability. If you're trying to evolve stability.


Interestingly, unlike many other proteins, Protein S is not in a "molten globule". It seems to have two distinct states, only differentiable by their inherent mechanical stability, but with no seeming different structure (since the Contour-length increment remains the same).

(Crystallizaiton occurs at lower pH to resolve clashes?)
Calcium provides a positive and negative role: it can create attraction and repulsion. A second domain can provide neutrality (by brining positive charge) to further provide modulation



CSV of Charge and Protein for supplement

Magnitude of charge and STRENGTH OF repulsion

Pros and cons of bistabiliy 
Pro: gives flexibility

Con: aggregate, crystalline may have evolved without for that reason
